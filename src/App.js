import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Provider } from 'react-redux';
import store from './redux/store';
import firebase from 'react-native-firebase';

import LoadingModal from './ui/components/LoadingModal';
import LoggedIn from './loggedIn/screens';
import LoggedOut from './loggedOut/screens';

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

export default class App extends React.Component {
  authSubscription: () => any;
  state: State;

  constructor() {
    super();
    this.state = {
      loading: true,
    };
  }

  componentDidMount() {
    this.authSubscription = firebase.auth().onAuthStateChanged((user) => {
      this.setState({
        loading: false,
        user,
      });
      firebase.analytics().setUserId(user ? user.uid : null);
    });
  }

  componentWillUnmount() {
    this.authSubscription();
  }

  render() {
    if (this.state.loading) return null;

    return (
      <Provider store={store}>
        <View style={styles.container}>
          <LoadingModal />
          {this.state.user ? <LoggedIn /> : <LoggedOut />}
        </View>
      </Provider>
    );
  }
}
