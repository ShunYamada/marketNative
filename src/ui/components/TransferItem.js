import React from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native';

import Icon from './Icon';
import * as Theme from '../../theme';

const styles = StyleSheet.create({
  container: {
    borderBottomWidth: 1,
    padding: 10,
    backgroundColor: '#fff',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderColor: '#eee',
    position: 'relative',
  },
  textContent: {
    flexDirection: 'column',
    width: 280
  },
  text: {
    fontSize: 14,
    width: 240,
    marginBottom: 10
  },
  thumbnail: {
    height: 50,
    width: 50,
    borderRadius: 25,
  },
  thumbnailContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 15
  },
  date: {
    color: Theme.BORDER_COLOR,
    fontSize: 12
  },
  price: {
    color: Theme.PRIMARY,
    fontWeight: 'bold',
  }
});

export default class TransferItem extends React.Component {
  render() {
    const { balance, createdAt } = this.props.transfer;
    return(
      <View>
        <View style={styles.container}>
          <View style={styles.textContent}>
            <Text style={styles.price}>{balance}円</Text>
            <Text style={styles.text}>出金申請が完了しました。</Text>
            <Text style={styles.date}>{createdAt}</Text>
          </View>
        </View>
      </View>
    );
  }
}
