import React, { type Node } from 'react';
import { StyleSheet, View } from 'react-native';

import * as Theme from '../../theme';

type Props = {
  children: Node,
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Theme.SCREEN_BACKGROUND_COLOR,
    flex: 1,
  },
});

export default (props: Props) => (
  <View style={[styles.container, props.style]}>
    {props.children}
  </View>
);
