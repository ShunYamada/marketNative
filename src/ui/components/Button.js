import React from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';

import type { StyleObj } from 'react-native/Libraries/StyleSheet/StyleSheetTypes';

import Icon from './Icon';
import * as Theme from '../../theme';

type Props = {
  containerStyle?: StyleObj,
  disabled?: boolean,
  icon?: string,
  iconStyle?: StyleObj,
  onPress: () => any,
  text?: string,
  textStyle?: StyleObj,
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    alignSelf: 'stretch',
    backgroundColor: Theme.PRIMARY,
    opacity: 0.8,
    borderRadius: 22.5,
    flexDirection: 'row',
    height: 45,
    justifyContent: 'center',
    margin: 4,
  },
  disabled: {
    backgroundColor: Theme.BUTTON_DISABLED,
  },
  icon: {
    color: '#fff',
    margin: 4,
    width: 45,
  },
  text: {
    color: '#fff',
    marginLeft: 8,
  },
});

export default (props: Props) => {
  const {
    containerStyle,
    disabled,
    icon,
    iconStyle,
    onPress,
    text,
    textStyle,
    ...restProps
  } = props;

  return (
    <TouchableOpacity
      disabled={disabled}
      onPress={onPress}
      style={[styles.container, disabled && styles.disabled, containerStyle]}
      {...restProps}>
      {icon && <Icon active name={icon} style={[styles.icon, iconStyle]} />}
      {text && <Text style={[styles.text, textStyle]}>{text}</Text>}
    </TouchableOpacity>
  );
};
