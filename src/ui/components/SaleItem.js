import React from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native';

import Icon from './Icon';
import * as Theme from '../../theme';

const styles = StyleSheet.create({
  container: {
    borderBottomWidth: 1,
    padding: 10,
    backgroundColor: '#fff',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderColor: '#eee',
    position: 'relative',
  },
  textContent: {
    flexDirection: 'column',
    width: 280
  },
  text: {
    fontSize: 14,
    width: 240,
    marginBottom: 10
  },
  thumbnail: {
    height: 50,
    width: 50,
    borderRadius: 25,
  },
  thumbnailContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 15
  },
  date: {
    color: Theme.BORDER_COLOR,
    fontSize: 12
  },
  price: {
    color: Theme.PRIMARY,
    fontWeight: 'bold',
  }
});

export default class SaleItem extends React.Component {
  render() {
    const { createdAt, host, item, guest } = this.props.deal;
    return(
      <View>
        <View style={styles.container}>
          <View
          style={styles.thumbnailContainer}>
            <Image
              style={styles.thumbnail}
              source={{uri: guest.photoURL}}
            />
          </View>
          <View style={styles.textContent}>
            <Text style={styles.price}>{item.price}円</Text>
            <Text style={styles.text}>{guest.name}が{item.name}を購入しました。</Text>
            <Text style={styles.date}>{createdAt}</Text>
          </View>
        </View>
      </View>
    );
  }
}
