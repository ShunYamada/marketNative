import React from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native';

import Icon from './Icon';

import * as Theme from '../../theme';

const styles = StyleSheet.create({
  container: {
    borderBottomWidth: 1,
    padding: 10,
    backgroundColor: '#fff',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderColor: '#eee',
    position: 'relative',
  },
  textContent: {
    flexDirection: 'column',
    width: 400
  },
  text: {
    fontSize: 14,
    width: 240,
    marginBottom: 10
  },
  thumbnail: {
    height: 50,
    width: 50,
    borderRadius: 25,
  },
  thumbnailContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 15
  },
  date: {
    color: Theme.BORDER_COLOR,
    fontSize: 12
  },
  content: {
    color: Theme.BORDER_COLOR_DARK
  },
  info: {
    width: 280,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  }
});

export default class MessageItem extends React.Component {
  render() {
    const { room } = this.props;
    const { user } = this.props.room;
    return(
      <TouchableOpacity
        onPress={this.props.onPress}
      >
        <View style={styles.container}>
          <View style={styles.thumbnailContainer}>
            <Image
              style={styles.thumbnail}
              source={{uri: user.photoURL }}
            />
          </View>
          <View style={styles.textContent}>
            <View style={styles.info}>
              <Text>{user.name}</Text>
              <Text style={styles.date}>{room.latest.createdAt}</Text>
            </View>
            <Text style={styles.content}>{room.latest.text}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}
