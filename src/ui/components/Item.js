import React from 'react';
import { Text, View, Image, TouchableOpacity, StyleSheet } from 'react-native';
import Card from './Card';
import CardSection from './CardSection';
import Button from './Button';
import Icon from './Icon';

import firebase from 'react-native-firebase';

import * as Theme from '../../theme';

const styles = StyleSheet.create({
  title: {
    fontSize: 16,
    marginBottom: 5
  },
  price: {
    fontSize: 14,
    fontWeight: 'bold'
  },
  image: {
    height: 200,
    flex: 1,
    width: null
  },
  thumbnail: {
    height: 50,
    width: 50,
    borderRadius: 25,
  },
  name: {
    fontSize: 16,
    marginLeft: 15,
    paddingTop: 15
  }
});

export default class PlanItem extends React.Component {
  render() {
    const {
      name,
      price,
      content,
      imageUrl,
      host,
    } = this.props.item;

    return (
      <TouchableOpacity onPress={this.props.onPress}>
        <Card>
          <CardSection>
            <View>
              <Text
                style={styles.title}
                numberOfLines={1}
                >
                {name}
              </Text>
              <Text
                style={styles.price}
                numberOfLines={1}
                >
                {price}円
              </Text>
            </View>
          </CardSection>
          <CardSection>
            <Image
              style={styles.image}
              source={{ uri: imageUrl }}
            />
          </CardSection>
          <CardSection>
            <Image
              style={styles.thumbnail}
              source={{ uri: host.photoURL }}
            />
            <Text numberOfLines={1} style={styles.name}>{host.name}</Text>
          </CardSection>
        </Card>
      </TouchableOpacity>
    );
  }
}
