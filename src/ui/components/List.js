import React from 'react';
import { StyleSheet, View } from 'react-native';

import * as Theme from '../../theme';

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    borderColor: Theme.BORDER_COLOR,
    borderTopWidth: StyleSheet.hairlineWidth,
    flex: 1,
    marginTop: 16,
  },
});

export default (props: Props) => (
  <View style={styles.container}>
    {props.children}
  </View>
);
