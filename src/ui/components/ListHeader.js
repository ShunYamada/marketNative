import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import * as Theme from '../../../theme';

const styles = StyleSheet.create({
  header: {
    backgroundColor: Theme.PRIMARY,
    borderColor: Theme.BORDER_COLOR_DARK,
    padding: 16,
  },
  headerText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: '500',
  },
});

export default (props: Props) => {
  const { text } = props;
  return (
    <View style={styles.header}>
      <Text style={styles.headerText}>{text}</Text>
    </View>
  );
};
