type uiActions = {
   type: 'message_update',
   type: 'message_create,'
 }

 const initialState = {
   text: '',
   user: '',
   createdAt: '',
   _id: '',
   roomId: ''
 };

 export default (state = initialState, action) => {
   switch(action.type) {
     case 'message_update':
       return { ...state, [action.payload.prop]: action.payload.value };
     case 'message_create':
       return initialState;
     default:
       return state;
   }
 };
