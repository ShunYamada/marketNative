type planActions = {
  type: 'item_update',
  type: 'item_create',
  type: 'item_save_success'
}

const initialState = {
  name: '',
  price: '',
  content: '',
  imageUrl: '',
  hostId: ''
};

export default (state = initialState, action) => {
  switch(action.type) {
    case 'item_update':
      return { ...state, [action.payload.prop]: action.payload.value };
    case 'item_create':
      return initialState;
    case 'item_save_success':
      return initialState;
    default:
      return state;
  }
};
