import _ from 'lodash';
import { Alert } from 'react-native';
import firebase from 'react-native-firebase';
import { formatDate, createdAt } from './uiActions';

export const roomUpdate = ({ prop, value }) => ({
   type: 'room_update',
   payload: { prop, value }
});

export const roomCreate = ({ host, text, navigation }) => {
  const { currentUser } = firebase.auth();

  return (dispatch) => {
    firebase.database().ref(`/rooms/${host.uid}/${currentUser.uid}`)
    .update({
      userId: currentUser.uid,
      latest: {
        text,
        createdAt: formatDate(new Date())
      }
    })
    firebase.database().ref(`/rooms/${currentUser.uid}/${host.uid}`)
    .update({
      userId: host.uid,
      latest: {
        text,
        createdAt: formatDate(new Date())
      }
    })
    .then(() => {
      firebase.database().ref(`/rooms/${currentUser.uid}/${room.userId}/messages/`)
      .push({
        _id: createdAtDate(new Date()),
        text: text,
        user: {
          _id: currentUser.uid,
          name: currentUser.name,
          photoURL: currentUser.photoURL
        },
        createdAt: formatDate(new Date()),
      })

      firebase.database().ref(`/rooms/${room.userId}/${currentUser.uid}/messages/`)
      .push({
        _id: createdAtDate(new Date()),
        text: text,
        user: {
          _id: currentUser.uid,
          name: currentUser.name,
          photoURL: currentUser.photoURL
        },
        createdAt: formatDate(new Date()),
      })
    })
    .then(() => {
      Alert.alert(
        'メッセージの送信に成功しました。',
        [
          {text: 'OK', onPress: console.log('Yes')},
        ],
        { cancelable: false }
      )
    })
  }
}

export const roomsFetch = () => {
  const { currentUser } = firebase.auth();
  return (dispatch) => {
    firebase.database().ref(`/rooms/${currentUser.uid}/`)
    .once('value', snapshot => {
      const rooms = snapshot.val();
      return rooms;
    })
    .then((rooms) => {
      const userPromises = _.map(rooms.val(), async (room, roomId) => {
        return firebase.database().ref(`/users/${room.userId}/`)
        .once('value')
          .then(snapshot => snapshot.val())
          .then(user => ({...room, user, roomId}));
      });
      dispatch({ type: 'ui/loading/hide' });
      return Promise.all(userPromises);
    })
    .then(roomItems => {
      dispatch({ type: 'rooms_fetch_success', payload: roomItems });
    });
  };
};
