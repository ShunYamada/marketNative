import _ from 'lodash';
import { Alert } from 'react-native';
import firebase from 'react-native-firebase';
import { PUBLIC_STRIPE_API } from '../../config';

const stripeUrl = 'https://api.stripe.com/v1/';

export const payUpdate = ({ prop, value }) => ({
 type: 'pay_update',
 payload: { prop, value }
});

export const createCardToken = (cardNumber, expiryMonth, expiryYear, cvc) => {
  const cardDetails = {
    'card[number]': cardNumber,
    'card[exp_month]': expiryMonth,
    'card[exp_year]': expiryYear,
    'card[cvc]': cvc,
  }

  const formBody = _.map(cardDetails, (value, key) => {
    const encodedValue = encodeURIComponent(value)
    const encodedKey = encodeURIComponent(key)
    return `${encodedKey}=${encodedValue}`
  }).join('&')

  return fetch(`${stripeUrl}tokens`, {
    method: 'post',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/x-www-form-urlencoded',
      Authorization: `Bearer ${PUBLIC_STRIPE_API}`,
    },
    body: formBody,
  });
};

export const payCreate = ({cardNumber, expiryMonth, expiryYear, cvc}) => {
  const { currentUser } = firebase.auth();

  return (dispatch) => {
    createCardToken(cardNumber, expiryMonth, expiryYear, cvc)
    .then(res => {
      console.log('res: ', res);
      if (!res.ok) {
        throw Error(res.type);
      }
      return res.json();
    })
    .then(token => {
      console.log('token: ', token);
      firebase.database().ref(`/users/${currentUser.uid}/card/`)
        .set({
          tokenId: token.id
        })
        .then(() => {
        dispatch({ type: 'pay_save_success' });
        Alert.alert(
          '支払い情報が保存されました',
          'カード情報が正常に保存されました。',
          [
            {text: 'OK', onPress: () => console.log('OK Pressed')},
          ],
          { cancelable: false }
        )
      });
    })
    .catch(err => {
      dispatch({ type: 'pay_save_failed' });
      Alert.alert(
        '支払い情報が保存されませんでした',
        'カード情報をもう一度確認の上、再度入力してください。',
        [
          {text: 'OK', onPress: () => console.log('OK Pressed')},
        ],
        { cancelable: false }
      )
    });
  }
}

export const payDelete = ({ uid, navigation }) => {
  const { currentUser } = firebase.auth();

  return () => {
    firebase.database().ref(`/users/${currentUser.uid}/card`)
      .remove()
      .then(() => {
        navigaion.navigate("Home");
      });
  };
};
