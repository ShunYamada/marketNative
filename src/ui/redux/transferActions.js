import _ from 'lodash';
import { Alert } from 'react-native';
import firebase from 'react-native-firebase';
import { formatDate, createdAtDate } from './uiActions';

export const transferUpdate = ({ prop, value }) => ({
 type: 'transfer_update',
 payload: { prop, value }
});

export const transferCreate = (user) => {
  return (dispatch) => {
    firebase.database().ref(`/transfers/${user.uid}`)
    .push({
      balance: user.balance,
      createdAt: formatDate(new Date()),
    })
    .then(() => {
      firebase.database().ref(`/users/${user.uid}`)
      .update({
        balance: "0",
      })
    });
  }
}

export const transfersFetch = () => {
  const { currentUser } = firebase.auth();
  return (dispatch) => {
    firebase.database().ref(`/transfers/${currentUser.uid}`)
    .once('value', snapshot => {
      dispatch({ type: 'transfers_fetch_success', payload: snapshot.val() });
    });
  };
};
