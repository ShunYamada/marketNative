import _ from 'lodash';
import firebase from 'react-native-firebase';

export const itemUpdate = ({ prop, value }) => ({
 type: 'item_update',
 payload: { prop, value }
});

export const itemCreate = ({ name, price, content, navigation, imageuri }) => {
  const { currentUser } = firebase.auth();
  const metadata = {
    contentType: 'image/jpeg'
  }
  const storageRef = firebase.storage().ref();
  const imagesRef = storageRef.child('images/plans' + currentUser.uid + Date.now() + '.jpeg');

  return (dispatch) => {
      imagesRef.putFile(imageuri, { contentType: 'image/jpeg' })
      .then((snapshot) =>{
      firebase.database().ref(`/items/`)
      .push({
        name,
        imageUrl: snapshot.downloadURL,
        price: Number(price),
        content,
        hostId: currentUser.uid
      })
      .then(() => {
        dispatch({ type: 'item_create' });
        navigation.goBack();
      });
    })
  };
};

export const itemsFetch = () => {
  return (dispatch) => {
    firebase.database().ref(`/items/`)
    .once('value', snapshot => {
      const items = snapshot.val();
      return items;
    })
    .then((items) => {
      const userPromises = _.map(items.val(), async (item, itemId) => {
        return firebase.database().ref(`/users/${item.hostId}`).once('value')
          .then(snapshot => snapshot.val())
          .then(user => ({...item, host: user, itemId}));
      });
      dispatch({ type: 'ui/loading/hide' });
      return Promise.all(userPromises);
    })
    .then(itemItems => {
      dispatch({ type: 'items_fetch_success', payload: itemItems });
    });
  };
};

export const saleFetch = () => {
  const { currentUser } = firebase.auth();
  return (dispatch) => {
    firebase.database().ref(`/items/`)
    .orderByChild('userId').equalTo(currentUser.uid)
    .once('value', snapshot => {
      const items = snapshot.val();
      return items;
    })
    .then((items) => {
      const userPromises = _.map(items.val(), async (item, itemId) => {
        return firebase.database().ref(`/users/${item.hostId}`).once('value')
          .then(snapshot => snapshot.val())
          .then(user => ({...item, host: user, itemId}));
      });
      dispatch({ type: 'ui/loading/hide' });
      return Promise.all(userPromises);
    })
    .then(itemItems => {
      dispatch({ type: 'items_fetch_success', payload: itemItems });
    });
  };
};

export const itemSave = ({ name, price, content, navigation, imageuri, itemId }) => {
  const { currentUser } = firebase.auth();
  const metadata = {
    contentType: 'image/jpeg'
  }
  const storageRef = firebase.storage().ref();
  const imagesRef = storageRef.child('images/plans' + currentUser.uid + Date.now() + '.jpeg');

  return (dispatch) => {
      imagesRef.putFile(imageuri, { contentType: 'image/jpeg' })
      .then((snapshot) =>{
      firebase.database().ref(`/items/${itemId}/`)
      .update({
        name,
        imageUrl: snapshot.downloadURL,
        price: Number(price),
        content,
        hostId: currentUser.uid
      })
      .then(() => {
        dispatch({ type: 'item_save_success' });
        navigation.goBack();
      });
    })
  };
};

export const textSave = ({ name, price, content, navigation, itemId }) => {
  const { currentUser } = firebase.auth();

  return (dispatch) => {
    firebase.database().ref(`/items/${itemId}/`)
    .update({
      name,
      price: Number(price),
      content,
      hostId: currentUser.uid
    })
    .then(() => {
      dispatch({ type: 'item_save_success' });
      navigation.goBack();
    });
  }
};

export const itemDelete = ({ itemId, navigation }) => {
  const { currentUser } = firebase.auth();

  return (dispatch) => {
  firebase.database().ref(`/items/${itemId}`)
   .remove()
   .then(() => {
     console.log('deleted!');
     navigation.navigate('Home');
   })
  }
}
