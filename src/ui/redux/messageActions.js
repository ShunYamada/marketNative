import _ from 'lodash';
import firebase from 'react-native-firebase';
import { formatDate, createdAtDate } from './uiActions';

export const messageUpdate = ({ prop, value }) => ({
 type: 'message_update',
 payload: { prop, value }
});

export const sendMessage = ({ message, room }) => {
  const { currentUser } = firebase.auth();
  const { receiver } = room.user;
  return (dispatch) => {
    firebase.database().ref(`/users/${currentUser.uid}/`)
    .once('value', snapshot => {
      const sender = snapshot.val();
      return sender;
    })
    .then((sender) => {
      firebase.database().ref(`/rooms/${currentUser.uid}/${room.userId}`)
      .update({
        latest: {
          text: message[0].text,
          createdAt: formatDate(new Date()),
        }
      })
      firebase.database().ref(`/rooms/${room.userId}/${currentUser.uid}`)
      .update({
        latest: {
          text: message[0].text,
          createdAt: formatDate(new Date()),
        }
      })

      firebase.database().ref(`/rooms/${currentUser.uid}/${room.userId}/messages/`)
      .push({
        _id: createdAtDate(new Date()),
        text: message[0].text,
        user: {
          _id: sender._value.uid,
          name: sender._value.name,
          photoURL: sender._value.photoURL
        },
        createdAt: formatDate(new Date()),
      })

      firebase.database().ref(`/rooms/${room.userId}/${currentUser.uid}/messages/`)
      .push({
        _id: createdAtDate(new Date()),
        text: message[0].text,
        user: {
          _id: sender._value.uid,
          name: sender._value.name,
          photoURL: sender._value.photoURL
        },
        createdAt: formatDate(new Date()),
      })
      .then(() => {
        dispatch({ type: 'message_create' });
      });
    })
  }
}

export const messagesFetch = (room) => {
  const { currentUser } = firebase.auth();
  return (dispatch) => {
    firebase.database().ref(`/rooms/${currentUser.uid}/${room.userId}/messages`)
    .orderByChild('_id')
    .on('value', snapshot => {
      dispatch({ type: 'messages_fetch_success', payload: snapshot.val() });
    })
  };
};
