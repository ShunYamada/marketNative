type uiActions = {
   type: 'deal_update',
   type: 'deal_create,'
 }

 const initialState = {
   item: '',
   host: '',
   guestId: '',
   createdAt: ''
 };

 export default (state = initialState, action) => {
   switch(action.type) {
     case 'deal_update':
       return { ...state, [action.payload.prop]: action.payload.value };
     case 'deal_create':
       return initialState;
     default:
       return state;
   }
 };
