type uiActions = {
  type: 'deals_fetch_success',
}

const initialState = {};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'deals_fetch_success':
      return action.payload;
    default:
      return state;
  }
};
