type payActions = {
  type: 'pay_update',
  type: 'pay_save_success',
  type: 'pay_save_failed'
}

const initialState = {
  cardNumber: '',
  expiryMonth: '',
  expiryYear: '',
  cvc: '',
  error: null
};

export default (state = initialState, action) => {
  switch(action.type) {
    case 'pay_update':
      return { ...state, [action.payload.prop]: action.payload.value };
    case 'pay_save_success':
      return initialState;
    case 'pay_save_failed':
      return { ...state, error: true };
    default:
      return state;
  }
};
