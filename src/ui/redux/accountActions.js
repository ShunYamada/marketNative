import _ from 'lodash';
import { Alert } from 'react-native';
import firebase from 'react-native-firebase';
import { formatDate } from './uiActions';

export const accountUpdate = ({ prop, value }) => ({
 type: 'account_update',
 payload: { prop, value }
});

export const accountCreate = ({ type, accountName, branch, number, name }) => {
  const { currentUser } = firebase.auth();

  return (dispatch) => {
    firebase.database().ref(`/users/${currentUser.uid}`)
    .once('value', snapshot => {
      const card = snapshot.val().card;
      firebase.database().ref(`/users/${currentUser.uid}/account/`)
        .set({
          type,
          accountName,
          branch,
          number,
          name,
        })
        .then(() => {
          dispatch({ type: 'account_save_success' });
          Alert.alert(
            '振込情報が保存されました',
            '銀行口座情報が正常に保存されました。',
            [
              {text: 'OK', onPress: () => console.log('OK Pressed')},
            ],
            { cancelable: false }
          )
        })
        .catch(err => {
          dispatch({ type: 'account_save_failed' });
          Alert.alert(
            '振込情報が保存されませんでした',
            '銀行口座情報をもう一度確認の上、再度入力してください。',
            [
              {text: 'OK', onPress: () => console.log('OK Pressed')},
            ],
            { cancelable: false }
          )
        });
    });
  }
}
