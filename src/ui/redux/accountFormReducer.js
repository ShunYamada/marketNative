type payActions = {
  type: 'account_update',
  type: 'account_save_success',
  type: 'account_save_failed'
}

const initialState = {
  type: '',
  accountName: '',
  branch: '',
  number: '',
  name: ''
};

export default (state = initialState, action) => {
  switch(action.type) {
    case 'account_update':
      return { ...state, [action.payload.prop]: action.payload.value };
    case 'account_save_success':
      return initialState;
    case 'account_save_failed':
      return { ...state, error: true };
    default:
      return state;
  }
};
