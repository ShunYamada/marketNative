import _ from 'lodash';
import { Alert } from 'react-native';
import firebase from 'react-native-firebase';
import { formatDate } from './uiActions';

export const createCharge = (amount, currency, tokenId) => {
  const chargeDetails = {
    amount,
    currency,
    source: tokenId
  };

  const formBody = _.map(chargeDetails, (value, key) => {
    const encodedValue = encodeURIComponent(value)
    const encodedKey = encodeURIComponent(key)
    return `${encodedKey}=${encodedValue}`
  }).join('&')

  return fetch(`${stripeUrl}charges`, {
    method: 'post',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/x-www-form-urlencoded',
      Authorization: `Bearer ${SECRET_STRIPE_API}`,
    },
    body: formBody,
  })
    .then(response => response.text())
    .then(responseText => {
      return JSON.parse(responseText);
    })
};

export const dealUpdate = ({ prop, value }) => ({
 type: 'deal_update',
 payload: { prop, value }
});

export const dealCreate = ({ item, host, navigation }) => {
  const { currentUser } = firebase.auth();

  if(owner.balance === undefined) {
    firebase.database().ref(`/users/${owner.uid}`)
    .update({
      balance: item.price
    });
  } else {
    firebase.database().ref(`/users/${owner.uid}`)
    .update({
      balance: Number(host.balance) + item.price
    });
  }

  return (dispatch) => {
    createCharge(item.price, 'jpy', user._value.card.tokenId)
    .then(token => {
      firebase.database().ref(`/deals/`)
      .push({
        item,
        host,
        itemId: item.uid,
        guestId: currentUser.uid,
        hostId: host.uid,
        createdAt: formatDate(new Date()),
        tokenId: token.id,
      })
      .then(() => {
        dispatch({ type: 'deal_create' });
        Alert.alert(
          '購入に成功しました。',
          'このアイテムを購入しました。',
          [
            {text: 'OK', onPress: () => console.log('OK')},
          ],
          { cancelable: false }
        )
      })
    });
  }
}

export const dealsFetch = () => {
  const { currentUser } = firebase.auth();
  return (dispatch) => {
    firebase.database().ref(`/deals/`)
    .orderByChild('hostId').equalTo(currentUser.uid)
    .once('value', snapshot => {
      const deals = snapshot.val();
      return deals;
    })
    .then((deals) => {
      const userPromises = _.map(deals.val(), async (deal, dealId) => {
        return firebase.database().ref(`/users/${deal.guestId}`).once('value')
          .then(snapshot => snapshot.val())
          .then(user => ({...deal, guest: user, dealId}));
      });
      dispatch({ type: 'ui/loading/hide' });
      return Promise.all(userPromises);
    })
    .then(dealItems => {
      dispatch({ type: 'deals_fetch_success', payload: dealItems });
    });
  };
};

export const purchaseFetch = () => {
  const { currentUser } = firebase.auth();
  return (dispatch) => {
    firebase.database().ref(`/deals/`)
    .orderByChild('guestId').equalTo(currentUser.uid)
    .once('value', snapshot => {
      const deals = snapshot.val();
      return deals;
    })
    .then((deals) => {
      const userPromises = _.map(deals.val(), async (deal, dealId) => {
        return firebase.database().ref(`/users/${deal.guestId}`).once('value')
          .then(snapshot => snapshot.val())
          .then(user => ({...deal, guest: user, dealId}));
      });
      dispatch({ type: 'ui/loading/hide' });
      return Promise.all(userPromises);
    })
    .then(dealItems => {
      dispatch({ type: 'deals_fetch_success', payload: dealItems });
    });
  };
};
