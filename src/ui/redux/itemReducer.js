type UiAction = {
  type: 'items_fetch_success',
}

const initialState = {};

export default (state = initialState, action) => {
 switch (action.type) {
   case 'items_fetch_success':
     return action.payload;
   default:
     return state;
 }
};
