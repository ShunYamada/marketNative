type uiActions = {
   type: 'room_update',
   type: 'room_create,'
 }

 const initialState = {
   text: '',
   createdAt: ''
 };

 export default (state = initialState, action) => {
   switch(action.type) {
     case 'room_update':
       return { ...state, [action.payload.prop]: action.payload.value };
     case 'room_create':
       return initialState;
     default:
       return state;
   }
 };
