export const PRIMARY = '#21ce99';
export const SECONDARY = '#63D6B3'

export const BORDER_COLOR = '#ccc';
export const BORDER_COLOR_DARK = '#999';

export const BUTTON = PRIMARY;
export const BUTTON_DISABLED = '#ccc';

export const TAB = '#ccc';

export const ERROR_COLOR = '#ed2f2f';

export const SCREEN_BACKGROUND_COLOR = '#fff';

export const TOAST_TEXT_COLOR = '#fff';
export const TOAST_ERROR_BACKGROUND_COLOR = ERROR_COLOR;
export const TOAST_MESSAGE_BACKGROUND_COLOR = 'rgba(0,0,0,0.8)';
export const TOAST_WARNING_BACKGROUND_COLOR = '#f0ad4e';
