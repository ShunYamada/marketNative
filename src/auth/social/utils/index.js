import { AccessToken, LoginManager } from 'react-native-fbsdk';
import firebase from 'react-native-firebase';

export const getFacebookLoginCredential = async (): Promise<?Object> => {
  let data = await AccessToken.getCurrentAccessToken();

  if (!data || !data.accessToken) {
    const result = await LoginManager.logInWithReadPermissions(['public_profile', 'email']);
    if (!result.isCancelled) {
      data = await AccessToken.getCurrentAccessToken();
    }
  }

  if (data && data.accessToken) {
    return firebase.auth.FacebookAuthProvider.credential(data.accessToken);
  }

  return null;
};
