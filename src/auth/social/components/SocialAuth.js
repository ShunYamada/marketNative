import React, { type Element } from 'react';
import firebase from 'react-native-firebase';
import { withNavigation } from 'react-navigation';
import { connect } from 'react-redux';

import type { NavigationScreenProp } from 'react-navigation/src/TypeDefinition';

import { getFacebookLoginCredential } from '../utils';
import { hideLoading, showLoading } from '../../../ui/redux/uiActions';
import { showError } from '../../../ui/components/Toast';

type Props = {
  dispatch: (Object) => any,
  navigation: NavigationScreenProp<*, *>,
  onSuccess: (Object, string) => any,
  providerId:
    firebase.auth.FacebookAuthProvider.PROVIDER_ID,
  render: (onPress: () => any) => Element<*>,
  type: 'link' | 'reAuth' | 'register' | 'signIn',
};

class SocialAuth extends React.Component<Props> {
  render() {
    return this.props.render(this.onLogin);
  }

  getCredential = async () => {
    const { providerId } = this.props;
    return getFacebookLoginCredential();

    throw new Error('Invalid providerId supplied');
  }

  onLogin = async () => {
    const {
      dispatch,
      navigation,
      onSuccess,
      type,
    } = this.props;

    try {
      dispatch(showLoading());
      const credential = await this.getCredential();

      if (credential) {
        let userCredential;
        if (type === 'link') {
          const { currentUser } = firebase.auth();
          if (!currentUser) {
            console.warn('Unexpected State: CurrentUser is unavailable');
            return;
          }
          userCredential = await currentUser.linkAndRetrieveDataWithCredential(credential);
        } else if (type === 'reAuth') {
          const { currentUser } = firebase.auth();
          if (!currentUser) {
            console.warn('Unexpected State: CurrentUser is unavailable');
            return;
          }
          userCredential =
            await currentUser.reauthenticateAndRetrieveDataWithCredential(credential);
        } else {
          userCredential = await firebase.auth().signInAndRetrieveDataWithCredential(credential);
        }

        if (onSuccess) onSuccess(userCredential.user, credential.providerId);
      }
    } catch (error) {
      if (type === 'link' && error.code === 'auth/requires-recent-login') {
        navigation.navigate('ReAuth');
      } else {
        showError(error.message);
      }
    }
    dispatch(hideLoading());
  }
}

export default connect()(withNavigation(SocialAuth));
