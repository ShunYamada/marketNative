import React from 'react';
import { TouchableOpacity, Text, StyleSheet } from 'react-native';
import { LoginManager } from 'react-native-fbsdk';
import firebase from 'react-native-firebase';

import * as Theme from '../../../theme';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    alignSelf: 'stretch',
    borderBottomWidth: 1,
    borderColor: Theme.BORDER_COLOR,
    flexDirection: 'row',
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  text: {
    flex: 1,
    marginTop: 5,
    marginBottom: 5
  }
});

const logout = () => {
  firebase.auth().signOut();
  LoginManager.logOut();
};

export default () => (
  <TouchableOpacity onPress={logout} style={styles.container}>
    <Text style={styles.text}>ログアウト</Text>
  </TouchableOpacity>
);
