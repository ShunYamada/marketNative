import _ from 'lodash';
import React from 'react';
import { StyleSheet, TouchableOpacity, View, Text, ListView } from 'react-native';
import firebase from 'react-native-firebase';
import { GiftedChat, Bubble, Send } from 'react-native-gifted-chat';

import { connect } from 'react-redux';
import { sendMessage, messagesFetch } from '../../ui/redux/messageActions';

import Screen from '../../ui/components/Screen';
import Icon from '../../ui/components/Icon';
import * as Theme from '../../theme';

const styles = StyleSheet.create({
  headerIcon: {
    paddingHorizontal: 15
  }
});

class Message extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: `${navigation.state.params.room.user.name}`,
      headerLeft: (
        <TouchableOpacity
          onPress={()=> navigation.goBack()}
          style={styles.headerIcon}
        >
          <Icon name='md-arrow-back' size={28} style={{color: '#fff'}}/>
        </TouchableOpacity>
      ),
      headerStyle: {
        backgroundColor: Theme.PRIMARY,
      },
      headerTitleStyle: {
        color: '#fff',
        fontWeight: 'bold'
      }
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      messages: [],
    };
  }

  componentWillMount() {
    const room = this.props.navigation.state.params.room;
    this.props.messagesFetch(room);
  }

  onSend(message) {
    const room = this.props.navigation.state.params.room;
    this.setState((previousState) => ({
      messages: GiftedChat.append(previousState.messages, message),
    }));

    this.props.sendMessage({
      message,
      room
    })
  }

  renderBubble(props) {
    return (
      <Bubble
        {...props}
        wrapperStyle={{
          right: {
            backgroundColor: Theme.SECONDARY
          }
        }}
      />
    )
  }

  renderSend(props) {
    return (
      <Send
      {...props}
      textStyle={{
        color: Theme.SECONDARY
      }}
      />
    );
  }

  render() {
    const { currentUser } = firebase.auth();
    return (
      <Screen>
        <GiftedChat
          messages={this.props.messages}
          onSend={this.onSend.bind(this)}
          user={{
            _id: currentUser.uid
          }}
          renderBubble={this.renderBubble}
          renderSend={this.renderSend}
        />
      </Screen>
    );
  }
}

const mapStateToProps = (state) => {
  const { text, user, createdAt, _id } = state.messageForm;
  const messages = _.orderBy(state.messages, ['_id'], ['desc']);
  return { text, user, createdAt, _id, messages };
};

export default connect(mapStateToProps, {
  sendMessage,
  messagesFetch
})(Message);
