import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Linking } from 'react-native';

import Screen from '../../ui/components/Screen';
import List from '../../ui/components/List';
import ListItem from '../../ui/components/ListItem';
import Icon from '../../ui/components/Icon';
import * as Theme from '../../theme';

import LogoutButton from '../../auth/core/components/LogoutButton';

const styles = StyleSheet.create({
  welcome: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
    padding: 24,
  },
  welcomeText: {
    fontSize: 20,
    marginTop: 24,
    textAlign: 'center',
  },
  headerIcon: {
    paddingHorizontal: 15
  },
  container: {
    alignItems: 'center',
    alignSelf: 'stretch',
    borderBottomWidth: 1,
    borderColor: Theme.BORDER_COLOR,
    flexDirection: 'row',
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  text: {
    flex: 1,
    color: Theme.ERROR_COLOR,
    marginTop: 5,
    marginBottom: 5
  }
});

export default class Setting extends React.Component<*> {
  static navigationOptions = ({ navigation }) => {
    return {
      title: '設定',
      headerStyle: {
        backgroundColor: Theme.PRIMARY,
      },
      headerTitleStyle: {
        color: '#fff',
        fontWeight: 'bold'
      },
      headerLeft: (
        <TouchableOpacity
          onPress={()=> navigation.goBack()}
          style={styles.headerIcon}
        >
          <Icon name='md-arrow-back' size={28} style={{color: '#fff'}}/>
        </TouchableOpacity>
      )
    };
  };

  render() {
    return (
      <Screen>
        <List>
          <ListItem
            text="支払い情報登録"
            onPress={()=>this.props.navigation.navigate('Payment')}
          />
          <ListItem
            text="振込情報登録"
            onPress={()=>this.props.navigation.navigate('Account')}
          />
          <ListItem
            text="出金申請"
            onPress={()=>this.props.navigation.navigate('Balance')}
          />
          <ListItem
            text="プライバシーポリシー"
          />
          <ListItem
            text="利用規約"
          />
          <ListItem
            onPress={this.onConfig}
            text="端末設定"
          />
          <ListItem
            text="利用規約"
          />
          <LogoutButton />
          <TouchableOpacity onPress={this.onDelete} style={styles.container}>
            <Text style={styles.text}>アカウントを削除する</Text>
          </TouchableOpacity>
        </List>
      </Screen>
    );
  }

  onConfig = () => {
    Linking.openURL('app-settings:');
  }

  onDelete = () => {
    Alert.alert(
      '本当にアカウントを削除しますか？',
      'この動作は取り消しできません。',
      [
        {text: 'キャンセル', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'はい', onPress: () => this.onDeleteConfirm()},
      ],
      { cancelable: false }
    )
  }

  onDeleteConfirm = () => {
    firebase.auth().currentUser.delete()
    .catch(function(error) {
      Alert.alert(
        'アカウントの削除ができませんでした。',
        'もう一度サインインし直してから実行してください。',
        [
          {text: 'OK', onPress: () => console.log('Okay')},
        ],
        { cancelable: false }
      )
    });
  }
}
