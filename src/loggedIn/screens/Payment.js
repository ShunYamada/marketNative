import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Alert } from 'react-native';
import firebase from 'react-native-firebase';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import { connect } from 'react-redux';
import { payUpdate, payCreate } from '../../ui/redux/payActions';

import Icon from '../../ui/components/Icon';
import Screen from '../../ui/components/Screen';
import CardSection from '../../ui/components/CardSection';
import Input from '../../ui/components/Input';
import * as Theme from '../../theme';

const styles = StyleSheet.create({
  headerIcon: {
    paddingHorizontal: 15
  },
  headerBtn: {
    color: '#fff',
    fontSize: 18,
    paddingRight: 15
  },
  added: {
    padding: 10,
    marginTop: 10,
    marginBottom: 10,
    marginRight: 15,
    marginLeft: 15,
    backgroundColor: Theme.SECONDARY
  },
  addedText: {
    color: '#fff',
    alignSelf: 'center'
  }
});

class Payment extends React.Component {
  constructor(props: Props, context: any) {
    super(props, context);
    // Set the default state of the component
    this.state = {
      showModal: false,
      error: null,
    }

    const { currentUser } = firebase.auth();
    firebase.database().ref(`/users/${currentUser.uid}`)
    .once('value', (snapshot) => {
      const user = snapshot.val();

      this.setState({
        card: user.card
      })
    })
  };

  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    return {
      headerTitle: 'お支払い情報',
      headerLeft: (
        <TouchableOpacity
          onPress={()=> navigation.goBack()}
          style={styles.headerIcon}
        >
          <Icon name='md-arrow-back' size={28} style={{color: '#fff'}}/>
        </TouchableOpacity>
      ),
      headerRight: (
        <TouchableOpacity
          color={'#fff'}
          onPress={() => params.handleSave()}
        >
         <Text style={styles.headerBtn}>保存する</Text>
        </TouchableOpacity>
      ),
      headerStyle: {
        backgroundColor: Theme.PRIMARY,
      },
      headerTitleStyle: {
        color: '#fff',
        fontWeight: 'bold'
      }
    };
  };

  onButtonPress() {
    const { cardNumber, expiryMonth, expiryYear, cvc } = this.props;
    this.props.payCreate({ cardNumber, expiryMonth, expiryYear, cvc });
  }

  onDone() {
    const { currentUser } = firebase.auth();
    firebase.database().ref(`/users/${currentUser.uid}/card`)
    .remove()
  }

  onDelete() {
    Alert.alert(
      '支払い情報を削除しますか？',
      'この動作は取り消しできません。',
      [
        {text: 'キャンセル', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'はい', onPress: () => this.onDone()},
      ],
      { cancelable: false }
    )
  }

  componentDidMount() {
    this.props.navigation.setParams({ handleSave: this.onButtonPress.bind(this) });
  }

  render() {
    const card = card;
    return (
      <Screen>
      { card === undefined ? (
        <KeyboardAwareScrollView>
          <CardSection>
            <Input
              label='クレジットカード番号'
              description={'クレジットカード番号を入力してください。'}
              placeholder='XXXXXXXXXXXXXXXX'
              secureTextEntry={true}
              value={this.props.cardNumber}
              maxLength={16}
              keyBoardType={'number-pad'}
              onChangeText={value => this.props.payUpdate({ prop: 'cardNumber', value })}
            />
          </CardSection>
          <CardSection>
            <Input
              label='有効期限(月)'
              description='有効月を入力してください。'
              placeholder='XX'
              value={this.props.expiryMonth}
              maxLength={2}
              keyBoardType={'number-pad'}
              onChangeText={value => this.props.payUpdate({ prop: 'expiryMonth', value })}
            />
          </CardSection>
          <CardSection>
            <Input
              label='有効期限(年)'
              placeholder='有効年を入力してください。'
              value={this.props.expiryYear}
              maxLength={2}
              keyBoardType={'number-pad'}
              onChangeText={value => this.props.payUpdate({ prop: 'expiryYear', value })}
            />
          </CardSection>
          <CardSection>
            <Input
              label='CVC'
              placeholder='カード裏面の3桁番号を入力してください。'
              value={this.props.cvc}
              maxLength={3}
              keyBoardType={'number-pad'}
              onChangeText={value => this.props.payUpdate({ prop: 'cvc', value })}
            />
          </CardSection>
        </KeyboardAwareScrollView>
      ) : (
        <TouchableOpacity style={styles.added} onPress={() => this.onDelete()}>
          <Text style={styles.addedText}>
          支払い情報は既に登録されています。{'\n'}
          情報を削除し、新規登録する場合はここをクリックしてください。</Text>
        </TouchableOpacity>
      )}
      </Screen>
    );
  }
}

const mapStateToProps = (state) => {
  const { cardNumber, expiryMonth, expiryYear, cvc } = state.payForm;

  return { cardNumber, expiryMonth, expiryYear, cvc };
};

export default connect(mapStateToProps, {
  payUpdate, payCreate
})(Payment);
