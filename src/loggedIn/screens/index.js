import React from 'react';
import { StyleSheet } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';

import Icon from '../../ui/components/Icon';
import * as Theme from '../../theme';

// screens
import Home from './Home';
import Detail from './Detail';
import Profile from './Profile';
import Notification from './Notification';
import MessageList from './MessageList';
import Message from './Message';
import Create from './Create';
import Edit from './Edit';
import Setting from './Setting';
import Payment from './Payment';
import Account from './Account';
import Balance from './Balance';
import ReAuthScreen from '../../auth/core/screens/ReAuthScreen';

type TabBarIcon = {
  tintColor: string,
};

const styles = StyleSheet.create({
  icon: {
    fontSize: 24,
  },
});

const CreateStack = createStackNavigator({
  Create: { screen: Create },
  Home: { screen: Home },
  Detail: { screen: Detail },
  Edit: { screen: Edit }
}, {
  innitialRouteName: 'Home',
  mode: 'modal',
})

const HomeStack = createStackNavigator({
  Home: { screen: CreateStack },
}, {
  initialRouteName: 'Home',
  headerMode: 'none'
});

const ProfileStack = createStackNavigator({
  Profile: { screen: Profile },
  Setting: { screen: Setting },
  Payment: { screen: Payment },
  Account: { screen: Account },
  Balance: { screen: Balance }
}, {
  initialRouteName: 'Profile',
});

const MessageStack = createStackNavigator({
  MessageList: { screen: MessageList },
  Message: { screen: Message }
}, {
  initialRouteName: 'MessageList',
});

const NotificationStack = createStackNavigator({
  Notification: { screen: Notification },
}, {
  initialRouteName: 'Notification',
});

const Tabs = createBottomTabNavigator({
  Home: {
    navigationOptions: {
      tabBarIcon: ({ tintColor }: TabBarIcon) => <Icon name="md-home" style={[styles.icon, { color: tintColor }]} />,
    },
    screen: HomeStack,
  },
  Message: {
    navigationOptions: {
      tabBarIcon: ({ tintColor }: TabBarIcon) => <Icon name="md-mail" style={[styles.icon, { color: tintColor }]} />,
    },
    screen: MessageStack,
  },
  Notification: {
    navigationOptions: {
      tabBarIcon: ({ tintColor }: TabBarIcon) => <Icon name="md-notifications" style={[styles.icon, { color: tintColor }]} />,
    },
    screen: NotificationStack,
  },
  Profile: {
    navigationOptions: {
      tabBarIcon: ({ tintColor }: TabBarIcon) => <Icon name="md-person" style={[styles.icon, { color: tintColor }]} />,
    },
    screen: ProfileStack,
  },
}, {
  initialRouteName: 'Home',
  tabBarOptions: {
    activeTintColor: Theme.PRIMARY,
    showLabel: false
  },
  tabBarPosition: 'bottom',
});

export default createStackNavigator({
  Tabs: { screen: Tabs },
  ReAuth: { screen: ReAuthScreen },
}, {
  cardStyle: {
    backgroundColor: 'transparent',
    opacity: 1,
  },
  headerMode: 'none',
  initialRouteName: 'Tabs',
  mode: 'modal',
});
