import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Alert } from 'react-native';
import firebase from 'react-native-firebase';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import { connect } from 'react-redux';
import { accountUpdate, accountCreate } from '../../ui/redux/accountActions';

import Icon from '../../ui/components/Icon';
import Screen from '../../ui/components/Screen';
import CardSection from '../../ui/components/CardSection';
import Input from '../../ui/components/Input';
import * as Theme from '../../theme';

const styles = StyleSheet.create({
  headerIcon: {
    paddingHorizontal: 15
  },
  headerBtn: {
    color: '#fff',
    fontSize: 18,
    paddingRight: 15
  },
  added: {
    padding: 10,
    marginTop: 10,
    marginBottom: 10,
    marginRight: 15,
    marginLeft: 15,
    backgroundColor: Theme.SECONDARY
  },
  addedText: {
    color: '#fff',
    alignSelf: 'center',
  }
});

class Account extends React.Component {
  constructor(props: Props, context: any) {
    super(props, context);
    // Set the default state of the component
    this.state = {
      showModal: false,
      error: null,
    }

    const { currentUser } = firebase.auth();
    firebase.database().ref(`/users/${currentUser.uid}`)
    .once('value', (snapshot) => {
      const user = snapshot.val();
      this.setState({
        account: user.account
      })
    })
  };

  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    return {
      headerTitle: '振込先情報',
      headerLeft: (
        <TouchableOpacity
          onPress={()=> navigation.goBack()}
          style={styles.headerIcon}
        >
          <Icon name='md-arrow-back' size={28} style={{color: '#fff'}}/>
        </TouchableOpacity>
      ),
      headerRight: (
        <TouchableOpacity
          color={'#fff'}
          onPress={() => params.handleSave()}
        >
         <Text style={styles.headerBtn}>保存する</Text>
        </TouchableOpacity>
      ),
      headerStyle: {
        backgroundColor: Theme.PRIMARY,
      },
      headerTitleStyle: {
        color: '#fff',
        fontWeight: 'bold'
      }
    };
  };

  onButtonPress() {
    const { type, accountName, branch, number, name } = this.props;
    this.props.accountCreate({ type, accountName, branch, number, name });
  }

  onDone() {
    const { currentUser } = firebase.auth();
    firebase.database().ref(`/users/${currentUser.uid}/account`)
    .remove()
  }

  onDelete() {
    Alert.alert(
      '振込情報を削除しますか？',
      'この動作は取り消しできません。',
      [
        {text: 'キャンセル', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'はい', onPress: () => this.onDone()},
      ],
      { cancelable: false }
    )
  }

  componentDidMount() {
    this.props.navigation.setParams({ handleSave: this.onButtonPress.bind(this) });
  }

  render() {
    const account = account;
    return (
      <Screen>
      { account === undefined ? (
        <KeyboardAwareScrollView>
          <CardSection>
            <Input
              label='口座種類'
              description={'口座の種類を入力してください。'}
              placeholder='普通'
              value={this.props.type}
              onChangeText={value => this.props.accountUpdate({ prop: 'type', value })}
            />
          </CardSection>
          <CardSection>
            <Input
              label='銀行名'
              description='銀行名を入力してください。'
              placeholder='XXX銀行'
              value={this.props.bankName}
              onChangeText={value => this.props.accountUpdate({ prop: 'accountName', value })}
            />
          </CardSection>
          <CardSection>
            <Input
              label='店番号'
              description='店番号を入力してください。'
              placeholder='XXX銀行'
              value={this.props.branch}
              onChangeText={value => this.props.accountUpdate({ prop: 'branch', value })}
            />
          </CardSection>
          <CardSection>
            <Input
              label='口座番号'
              description='口座番号を入力してください。'
              placeholder='XXXXXXX'
              maxLength={7}
              value={this.props.number}
              onChangeText={value => this.props.accountUpdate({ prop: 'number', value })}
            />
          </CardSection>
          <CardSection>
            <Input
              label='受取人名'
              description='受取人名を入力してください。'
              placeholder='ヤマダタロウ'
              value={this.props.name}
              onChangeText={value => this.props.accountUpdate({ prop: 'name', value })}
            />
          </CardSection>
        </KeyboardAwareScrollView>
      ) : (
        <TouchableOpacity style={styles.added} onPress={() => this.onDelete()}>
          <Text style={styles.addedText}>
            口座情報は既に登録されています。{'\n'}
            情報を削除し、新規登録する場合はここをクリックしてください。
          </Text>
        </TouchableOpacity>
      )}
      </Screen>
    );
  }
}

const mapStateToProps = (state) => {
  const { type, accountName, branch, number, name } = state.accountForm;

  return { type, accountName, branch, number, name };
};

export default connect(mapStateToProps, {
  accountUpdate, accountCreate
})(Account);
