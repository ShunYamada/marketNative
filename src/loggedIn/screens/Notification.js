import _ from 'lodash';
import React from 'react';
import { ListView, StyleSheet, TouchableOpacity, RefreshControl } from 'react-native';

import { connect } from 'react-redux';
import { dealsFetch } from '../../ui/redux/dealActions';
import { hideLoading, showLoading } from '../../ui/redux/uiActions';

import Icon from '../../ui/components/Icon';
import Screen from '../../ui/components/Screen';
import NotificationItem from '../../ui/components/NotificationItem';

import * as Theme from '../../theme';

const styles = StyleSheet.create({
  headerIcon: {
    paddingHorizontal: 15
  }
});

class Notification extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: '通知',
      headerStyle: {
        backgroundColor: Theme.PRIMARY,
      },
      headerTitleStyle: {
        color: '#fff',
        fontWeight: 'bold'
      }
    };
  };

  componentWillMount() {
    this.props.dealsFetch();

    this.createDataSource(this.props);
  }

  componentWillReceiveProps(nextProps) {

    this.createDataSource(nextProps);
  }

  createDataSource({ deals }) {
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });

    this.dataSource = ds.cloneWithRows(deals);
  }

  renderRow(deal) {
    return <NotificationItem deal={deal} />
  }

  _onRefresh() {
    this.props.showLoading();
    this.props.dealsFetch();
  }

  render() {
    console.log('jjj', this.props);
    return (
      <Screen>
        <ListView
          enableEmptySections
          refreshControl={
            <RefreshControl
              refreshing={this.props.ui.loading}
              onRefresh={this._onRefresh.bind(this)}
            />
          }
          dataSource={this.dataSource}
          renderRow={this.renderRow.bind(this)}
        />
      </Screen>
    );
  }
}

const mapStateToProps = state => {
  const deals = _.map(state.deals, (val, uid) => {
    return { ...val, uid };
  });
  const ui = state.ui;

  return { deals, ui };
}

export default connect(mapStateToProps, {
  dealsFetch,
  hideLoading,
  showLoading
})(Notification);
