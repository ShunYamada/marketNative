import _ from 'lodash';
import React from 'react';
import firebase from 'react-native-firebase';
import { StyleSheet, View, Text, TouchableOpacity, ScrollView, ListView, RefreshControl, Alert } from 'react-native';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import Modal from "react-native-modal";

import { connect } from 'react-redux';
import { transferCreate, transfersFetch } from '../../ui/redux/transferActions';
import { dealsFetch } from '../../ui/redux/dealActions';
import { hideLoading, showLoading } from '../../ui/redux/uiActions';

import Screen from '../../ui/components/Screen';
import SaleItem from '../../ui/components/SaleItem';
import TransferItem from '../../ui/components/TransferItem';
import Card from '../../ui/components/Card';
import CardSection from '../../ui/components/CardSection';
import Icon from '../../ui/components/Icon';
import * as Theme from '../../theme';

const styles = StyleSheet.create({
  headerIcon: {
    paddingHorizontal: 15
  },
  container: {
    backgroundColor: Theme.PRIMARY,
    paddingTop: 10,
    paddingBottom: 30
  },
  balanceText: {
    color: '#fff',
    fontSize: 32,
    fontWeight: 'bold',
    alignSelf: 'center',
    marginBottom: 5
  },
  description: {
    color: '#fff',
    fontSize: 14,
    alignSelf: 'center',
    marginBottom: 20
  },
  button: {
    alignSelf: 'center',
    backgroundColor: '#fff',
    borderRadius: 5,
    paddingTop: 5,
    paddingBottom: 5,
    paddingRight: 10,
    paddingLeft: 10
  },
  buttonText: {
    color: Theme.PRIMARY
  }
});

class Balance extends React.Component {
  constructor(props: Props, context: any) {
    super(props, context);
    // Set the default state of the component
    this.state = {
      user: [],
      showModal: false
    }

    const { currentUser } = firebase.auth();

    firebase.database().ref(`/users/${currentUser.uid}`)
    .once('value', (snapshot) => {
      const userId = snapshot.val();
      this.setState({
        user: userId
      })
    })
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: '',
      headerLeft: (
        <TouchableOpacity
          onPress={()=> navigation.goBack()}
          style={styles.headerIcon}
        >
          <Icon name='md-arrow-back' size={28} style={{color: '#fff'}}/>
        </TouchableOpacity>
      ),
      headerStyle: {
        backgroundColor: Theme.PRIMARY,
      },
      headerTitleStyle: {
        color: '#fff',
        fontWeight: 'bold'
      }
    };
  };

  componentWillMount() {
    this.props.transfersFetch();
    this.props.dealsFetch();
    this.createTransferSource(this.props);
    this.createSaleSource(this.props);
  }

  renderTransfer(transfer) {
    return <TransferItem transfer={transfer} />;
  }

  renderSale(deal) {
    return <SaleItem deal={deal} />;
  }

  componentWillReceiveProps(nextProps) {
    this.createSaleSource(nextProps)
  }

  createTransferSource({ transfers }) {
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });
    this.dataTransfers = ds.cloneWithRows(transfers);
  }

  createSaleSource({ deals }) {
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });
    this.dataSales = ds.cloneWithRows(deals);
  }

  _onTransfers() {
    this.props.showLoading();
    this.props.transfersFetch();
  }

  _onSales() {
    this.props.showLoading();
    this.props.dealsFetch();
  }

  onButtonPress() {
    const { user } = this.state;

    if(user.balance < 10000) {
      Alert.alert(
        '出金最低金額に達していません',
        '残高は最低10,000円より出金が可能です。',
        [
          {text: 'OK', onPress: () => console.log('Ok pressed!')},
        ],
        { cancelable: false }
      )
    } else if(user.bank == undefined) {
      Alert.alert(
        '振込先情報が未登録です',
        '設定より振込先情報を登録してください。',
        [
          {text: 'OK', onPress: () => console.log('OK')},
        ],
        { cancelable: false }
      )
    } else {
      this.setState({ showModal: !this.state.showModal })
    }
  }

  onAccept() {
    const { user } = this.state;
    const { navigation } = this.props;

    this.props.transferCreate(user);
  }

  onDecline() {
    this.setState({ showModal: false });
  }

  render() {
    const { user } = this.state;
    return (
      <Screen>
        <View style={styles.container}>
          { user.balance === undefined ?
          <Text style={styles.balanceText}>0円</Text>
          :
          <Text style={styles.balanceText}>{user.balance}円</Text>
          }
          <Text style={styles.description}>残高</Text>
          <TouchableOpacity style={styles.button} onPress={this.onButtonPress.bind(this)}>
            <Text style={styles.buttonText}>出金申請する</Text>
          </TouchableOpacity>
        </View>

        <ScrollableTabView
          style={{marginTop: 20}}
        >
          <View tabLabel='出金履歴'>
            <ListView
              enableEmptySections
              refreshControl={
                <RefreshControl
                  refreshing={this.props.ui.loading}
                  onRefresh={this._onTransfers.bind(this)}
                />
              }
              dataSource={this.dataTransfers}
              renderRow={this.renderTransfer.bind(this)}
            />
          </View>
          <View tabLabel='売上履歴'>
            <ListView
              enableEmptySections
              refreshControl={
                <RefreshControl
                  refreshing={this.props.ui.loading}
                  onRefresh={this._onSales.bind(this)}
                />
              }
              dataSource={this.dataSales}
              renderRow={this.renderSale.bind(this)}
            />
          </View>
        </ScrollableTabView>
        <Modal>
          <CardSection style={{ justifyContent: 'center' }}>
            <Text style={styles.confirmModal}>'本当に出金しますか？'</Text>
          </CardSection>
        </Modal>
      </Screen>
    );
  }
}


const mapStateToProps = state => {
  const deals = _.map(state.deals, (val, uid) => {
    return { ...val, uid };
  });
  const transfers = _.map(state.transfers, (val, uid) => {
    return { ...val, uid };
  });
  const ui = state.ui;
  return { deals, transfers, ui };
};

export default connect (mapStateToProps, {
  dealsFetch,
  transferCreate,
  transfersFetch,
  hideLoading,
  showLoading
})(Balance);
