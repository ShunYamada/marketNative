import _ from 'lodash';
import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Image, ListView, RefreshControl } from 'react-native';
import firebase from 'react-native-firebase';
import ScrollableTabView from 'react-native-scrollable-tab-view';

import { connect } from 'react-redux';
import { saleFetch } from '../../ui/redux/itemActions';
import { purchaseFetch } from '../../ui/redux/dealActions';
import { showLoading, hideLoading } from '../../ui/redux/uiActions';

import Item from '../../ui/components/Item';
import Screen from '../../ui/components/Screen';
import Icon from '../../ui/components/Icon';
import * as Theme from '../../theme';

const styles = StyleSheet.create({
  headerIcon: {
    paddingHorizontal: 15
  },
  profileContainer: {
    height: 200,
    backgroundColor: '#EEE',
    justifyContent: 'center',
    alignItems: 'center'
  },
  photo: {
    width: 80,
    height: 80,
    borderRadius: 40,
    marginBottom: 15
  }
});

class Profile extends React.Component<*> {
  constructor(props: Props, context: any) {
    super(props, context);
    this.state = {
      user: [],
    }
    const { currentUser } = firebase.auth();
    firebase.database().ref(`/users/${currentUser.uid}`)
    .once('value', (snapshot) => {
      const userId = snapshot.val();

      this.setState({
        user: userId
      })
    })
  }

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'プロフィール',
      headerStyle: {
        backgroundColor: Theme.PRIMARY,
      },
      headerTitleStyle: {
        color: '#fff',
        fontWeight: 'bold'
      },
      headerLeft: (
        <TouchableOpacity
          onPress={()=> navigation.navigate('Setting')}
          style={styles.headerIcon}
        >
          <Icon name='md-settings' size={28} style={{color: '#fff'}}/>
        </TouchableOpacity>
      ),
    };
  };

  componentWillMount() {
    this.props.saleFetch();
    this.props.purchaseFetch();
    this.createSaleSource(this.props);
    this.createPurchaseSource(this.props);
  }

  renderSale(item) {
    return <Item item={item} onPress={()=>this.props.navigation.navigate('Detail', {item})} />;
  }

  renderPurchase(deal) {
    return <Item item={deal} />;
  }

  componentWillReceiveProps(nextProps) {
    this.createSaleSource(nextProps);
    this.createPurchaseSource(nextProps)
  }

  createSaleSource({ items }) {
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });
    this.saleSource = ds.cloneWithRows(items);
  }

  createPurchaseSource({ deals }) {
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });
    this.purchaseSource = ds.cloneWithRows(deals);
  }

  _onSale() {
    this.props.showLoading();
    this.props.saleFetch();
  }

  _onPurchase() {
    this.props.showLoading();
    this.props.purchaseFetch();
  }

  render() {
    const { user } = this.state;

    if (!user) {
      return null;
    }
    return (
      <Screen>
        <View style={styles.profileContainer}>
          <Image
            source={{ uri: user.photoURL }}
            style={styles.photo}
          />
          <Text>{user.name}</Text>
        </View>

        <ScrollableTabView
          style={{marginTop: 20}}
        >
          <View tabLabel='販売履歴'>
            <ListView
              enableEmptySections
              refreshControl={
                <RefreshControl
                  refreshing={this.props.ui.loading}
                  onRefresh={this._onSale.bind(this)}
                />
              }
              dataSource={this.saleSource}
              renderRow={this.renderSale.bind(this)}
            />
          </View>
          <View tabLabel='購入履歴'>
            <ListView
              enableEmptySections
              refreshControl={
                <RefreshControl
                  refreshing={this.props.ui.loading}
                  onRefresh={this._onPurchase.bind(this)}
                />
              }
              dataSource={this.purchaseSource}
              renderRow={this.renderPurchase.bind(this)}
            />
          </View>
        </ScrollableTabView>
      </Screen>
    );
  }
}

const mapStateToProps = state => {
  const items = _.map(state.items, (val, uid) => {
    return { ...val, uid };
  });
  const deals = _.map(state.deals, (val, uid) => {
    return { ...val, uid };
  });
  const ui = state.ui;

  return { items, deals, ui };
}

export default connect(mapStateToProps, {
  saleFetch,
  purchaseFetch,
  hideLoading,
  showLoading
})(Profile);
