import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Image, ScrollView, Alert, TextInput, Button } from 'react-native';
import firebase from 'react-native-firebase';
import Modal from "react-native-modal";

import { connect } from 'react-redux';
import { itemDelete } from '../../ui/redux/itemActions';
import { dealCreate } from '../../ui/redux/dealActions';
import { roomUpdate, roomCreate } from '../../ui/redux/roomActions';

import Icon from '../../ui/components/Icon';
import CardSection from '../../ui/components/CardSection';
import * as Theme from '../../theme';

const styles = StyleSheet.create({
  welcome: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
    padding: 24,
  },
  welcomeText: {
    fontSize: 20,
    marginTop: 24,
    textAlign: 'center',
  },
  headerIcon: {
    paddingHorizontal: 15
  },
  image: {
    height: 280,
    flex: 1,
    width: null,
    margin: -10
  },
  title: {
    fontSize: 16,
    marginBottom: 5
  },
  price: {
    fontSize: 14,
    fontWeight: 'bold'
  },
  thumbnail: {
    height: 50,
    width: 50,
    borderRadius: 25,
  },
  name: {
    fontSize: 16,
    marginLeft: 15,
    paddingTop: 15
  },
  facebook: {
    backgroundColor: '#4267b2',
    marginTop: 10,
    marginBottom: 10,
    width: '100%'
  },
  icon: {
    fontSize: 28,
    marginRight: 0
  },
  container: {
    alignItems: 'center',
    alignSelf: 'stretch',
    backgroundColor: Theme.PRIMARY,
    opacity: 0.8,
    flexDirection: 'row',
    height: 45,
    justifyContent: 'center',
  },
  text: {
    color: '#fff',
    marginLeft: 8,
  },
  modal: {
    height: 230,
    backgroundColor: '#fff',
    alignItems: 'center',
    padding: 24
  },
  input: {
    width: '100%',
    marginTop: 15,
    marginBottom: 15,
    height: 100
  },
  button: {
    alignItems: 'center',
    alignSelf: 'stretch',
    backgroundColor: Theme.PRIMARY,
    opacity: 0.8,
    flexDirection: 'row',
    height: 40,
    justifyContent: 'center',
    borderRadius: 10
  },
  delete: {
    alignItems: 'center',
    alignSelf: 'stretch',
    backgroundColor: Theme.TOAST_ERROR_BACKGROUND_COLOR,
    opacity: 0.8,
    flexDirection: 'row',
    height: 45,
    justifyContent: 'center',
  }
});

class Detail extends React.Component<*> {
  constructor(props: Props, context: any) {
    super(props, context);
    this.state = {
      isModalVisible: false
    }
  };

  _toggleModal = () =>
    this.setState({ isModalVisible: !this.state.isModalVisible });

  onPurchase() {
    if(user.card == undefined) {
      Alert.alert(
        '支払い情報が登録されていません',
        '支払い情報を登録してから、再度購入してください。',
        [
          {text: 'OK', onPress: () => console.log('OK')},
        ],
        { cancelable: false }
      )
    } else {
      Alert.alert(
        '本当にこの商品を購入しますか？',
        'この動作は取り消しできません。',
        [
          {text: 'キャンセル', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
          {text: 'はい', onPress: this.onAccept.bind(this)},
        ],
        { cancelable: false }
      )
    }
  }

  onDelete() {
    Alert.alert(
      '本当にこの商品を削除しますか？',
      'この動作は取り消しできません。',
      [
        {text: 'キャンセル', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'はい', onPress: this.onDone.bind(this)},
      ],
      { cancelable: false }
    )
  }

  onMessage() {
    const item = this.props.navigation.state.params.item;
    const props = {
      text: this.props.text,
      host: this.props.navigation.state.params.item.host,
      navigation: this.props.navigation,
    };
    this.props.roomCreate(props);
  }

  onAccept() {
    const item = this.props.navigation.state.params.item;
    const props = {
      item: this.props.navigation.state.params.item,
      host: this.props.navigation.state.params.item.host,
      navigation: this.props.navigation,
    };
    this.props.dealCreate(props);
  }

  onDone() {
    const { itemId } = this.props.navigation.state.params.item;
    this.props.itemDelete({ itemId, navigation: this.props.navigation });
  }

  static navigationOptions = ({ navigation }) => {
    return {
      title: '詳細',
      headerStyle: {
        backgroundColor: Theme.PRIMARY,
      },
      headerTitleStyle: {
        color: '#fff',
        fontWeight: 'bold'
      },
      headerLeft: (
        <TouchableOpacity
          onPress={()=> navigation.goBack()}
          style={styles.headerIcon}
        >
          <Icon name='md-close' size={28} style={{color: '#fff'}}/>
        </TouchableOpacity>
      ),
    };
  };

  render() {
    const item = this.props.navigation.state.params.item;
    const { currentUser } = firebase.auth();
    return (
      <ScrollView>
        <CardSection>
          <Image
            source={{ uri: item.imageUrl }}
            style={styles.image}
          />
        </CardSection>

        <CardSection>
          <View>
            <Text
              style={styles.title}
              numberOfLines={1}
              >
              {item.name}
            </Text>
          </View>
        </CardSection>

        <TouchableOpacity
          onPress={this._toggleModal}
          >
          <CardSection>
            <Image
              style={styles.thumbnail}
              source={{ uri: item.host.photoURL }}
            />
            <Text numberOfLines={1} style={styles.name}>{item.host.name}</Text>
          </CardSection>
        </TouchableOpacity>

        <CardSection>
          <Text>{item.content}</Text>
        </CardSection>

        { item.host.id === currentUser.uid ? (
          <TouchableOpacity
            style={styles.container}
            onPress={this.onPurchase.bind(this)}
            >
            <Text style={styles.text}>この商品を購入する</Text>
          </TouchableOpacity>
        ) : (
          <View>
            <TouchableOpacity
              style={styles.container}
              onPress={()=> this.props.navigation.navigate('Edit', {item})}
              >
              <Text style={styles.text}>アイテムを編集する</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.delete}
              onPress={this.onDelete.bind(this)}
              >
              <Text style={styles.text}>アイテムを削除する</Text>
            </TouchableOpacity>
          </View>
        )}

        <Modal
          isVisible={this.state.isModalVisible}
          onBackdropPress={this._toggleModal}
          >
          <View style={styles.modal}>
            <Text>ホストにメッセージを送りましょう</Text>
            <TextInput
              style={styles.input}
              multiline
              blurOnSubmit={false}
              placeholder={'メッセージを入力してください'}
              value={this.props.text}
              onChangeText={value => this.props.roomUpdate({ prop: 'text', value })}
            />
            <TouchableOpacity
              style={styles.button}
              onPress={this.onMessage.bind(this)}
              >
              <Text style={styles.text}>メッセージを送信する</Text>
            </TouchableOpacity>
          </View>
        </Modal>
      </ScrollView>
    );
  }
}

const mapStateToProps = (state) => {
  const { item, host } = state.dealForm;
  const { text } = state.roomForm;

  return { item, host, text };
};

export default connect (mapStateToProps, {
  dealCreate,
  roomUpdate,
  roomCreate,
  itemDelete
})(Detail);
