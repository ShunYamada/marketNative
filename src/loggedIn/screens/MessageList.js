
import _ from 'lodash';
import React from 'react';
import { ListView, StyleSheet, Text, TouchableOpacity, RefreshControl } from 'react-native';

import { connect } from 'react-redux';
import { roomsFetch } from '../../ui/redux/roomActions';
import { hideLoading, showLoading } from '../../ui/redux/uiActions';

import Icon from '../../ui/components/Icon';
import * as Theme from '../../theme';
import Screen from '../../ui/components/Screen';
import MessageItem from '../../ui/components/MessageItem';

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  headerIcon: {
    paddingHorizontal: 15
  },
});

class MessageList extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: 'メッセージ一覧',
      headerStyle: {
        backgroundColor: Theme.PRIMARY,
      },
      headerTitleStyle: {
        color: '#fff',
        fontWeight: 'bold'
      }
    };
  };

  componentWillMount() {
    this.props.roomsFetch();

    this.createDataSource(this.props);
  }

  componentWillReceiveProps(nextProps) {

    this.createDataSource(nextProps);
  }

  createDataSource({ rooms }) {
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });

    this.dataSource = ds.cloneWithRows(rooms);
  }

  renderRow(room) {
    return <MessageItem room={room} onPress={()=>this.props.navigation.navigate('Message', {room})} />
  }

  _onRefresh() {
    this.props.showLoading();
    this.props.roomsFetch();
  }

  render() {
    console.log('messages', this.props);
    return (
      <Screen>
        <ListView
          enableEmptySections
          refreshControl={
            <RefreshControl
              refreshing={this.props.ui.loading}
              onRefresh={this._onRefresh.bind(this)}
            />
          }
          dataSource={this.dataSource}
          renderRow={this.renderRow.bind(this)}
        />
      </Screen>
    );
  }
}

const mapStateToProps = state => {
  const rooms = _.orderBy(state.rooms, ['uid'], ['desc']);
  const ui = state.ui;

  return { rooms, ui };
}

export default connect(mapStateToProps, {
  roomsFetch,
  hideLoading,
  showLoading
})(MessageList);
