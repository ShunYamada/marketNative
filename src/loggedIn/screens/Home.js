import _ from 'lodash';

import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, RefreshControl, ListView } from 'react-native';
import firebase from 'react-native-firebase';

import { connect } from 'react-redux';
import { itemsFetch } from '../../ui/redux/itemActions';
import { showLoading, hideLoading } from '../../ui/redux/uiActions';

import Screen from '../../ui/components/Screen';
import Item from '../../ui/components/Item';
import Icon from '../../ui/components/Icon';
import * as Theme from '../../theme';

const styles = StyleSheet.create({
  headerIcon: {
    paddingHorizontal: 15
  }
});

class Home extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'ホーム',
      headerStyle: {
        backgroundColor: Theme.PRIMARY,
      },
      headerTitleStyle: {
        color: '#fff',
        fontWeight: 'bold'
      },
      headerRight: (
        <TouchableOpacity
          color={'#fff'}
          onPress={() => navigation.navigate('Create')}
          style={styles.headerIcon}
        >
        <Icon name='md-add' size={28} style={{color: '#fff'}}/>
        </TouchableOpacity>
      ),
    };
  };

  componentWillMount() {
    this.props.itemsFetch();

    this.createDataSource(this.props);
  }

  renderRow(item) {
    return <Item item={item} onPress={()=>this.props.navigation.navigate('Detail', {item})}/>;
  }

  componentWillReceiveProps(nextProps) {
    this.createDataSource(nextProps);
  }

  createDataSource({ items }) {
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });

    this.dataSource = ds.cloneWithRows(items);
  }

  _onRefresh() {
    this.props.showLoading();
    this.props.itemsFetch();
  }

  render() {
    return (
      <Screen>
        <ListView
          enableEmptySections
          refreshControl={
            <RefreshControl
              style={{ backgroundColor: 'transparent' }}
              refreshing={this.props.ui.loading}
              onRefresh={this._onRefresh.bind(this)}
            />
          }
          dataSource={this.dataSource}
          renderRow={this.renderRow.bind(this)}
        />
      </Screen>
    );
  }
}

const mapStateToProps = state => {
  const items = _.map(state.items, (val, uid) => {
    return { ...val, uid };
  });
  const ui = state.ui;

  return { items, ui };
}

export default connect(mapStateToProps, {
  itemsFetch,
  hideLoading,
  showLoading
})(Home);
