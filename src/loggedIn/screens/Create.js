import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Alert, Image } from 'react-native';
import ImagePicker from 'react-native-image-picker';
import ImageResizer from 'react-native-image-resizer';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import { connect } from 'react-redux';
import { itemUpdate, itemCreate } from '../../ui/redux/itemActions';

import Screen from '../../ui/components/Screen';
import Card from '../../ui/components/Card';
import CardSection from '../../ui/components/CardSection';
import Input from '../../ui/components/Input';
import Box from '../../ui/components/Box';

import Icon from '../../ui/components/Icon';
import * as Theme from '../../theme';

const styles = StyleSheet.create({
  welcome: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
    padding: 24,
  },
  welcomeText: {
    fontSize: 20,
    marginTop: 24,
    textAlign: 'center',
  },
  headerIcon: {
    paddingHorizontal: 15
  },
  headerBtn: {
    color: '#fff',
    fontSize: 18,
    paddingRight: 15
  },
  image: {
    height: 180,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#eee',
    width: 360,
  },
  sample: {
    width: 360,
    height: 180,
  },
});

class Create extends React.Component<*> {
  constructor(props) {
     super(props);
     this.state = {
       ImageSource: null
     }
   }

   onButtonPress() {
     if( this.state.ImageSource === null ) {
       Alert.alert('画像を追加してください');
     } else {
       const props = {
         name: this.props.name,
         imageuri: this.state.ImageSource.uri,
         price: this.props.price,
         content: this.props.content,
         navigation: this.props.navigation,
       };
       this.props.itemCreate(props);
     }
   }

   async componentDidMount() {
     this.props.navigation.setParams({
       handleSave: this.onButtonPress.bind(this)
     });
   }

  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    return {
      title: 'アイテムの作成',
      headerStyle: {
        backgroundColor: Theme.PRIMARY,
      },
      headerTitleStyle: {
        color: '#fff',
        fontWeight: 'bold'
      },
      headerLeft: (
        <TouchableOpacity
          onPress={()=> navigation.goBack()}
          style={styles.headerIcon}
        >
          <Icon name='md-close' size={28} style={{color: '#fff'}}/>
        </TouchableOpacity>
      ),
      headerRight: (
        <TouchableOpacity
          color={'#fff'}
          onPress={() => params.handleSave()}
        >
         <Text style={styles.headerBtn}>投稿する</Text>
        </TouchableOpacity>
      ),
      headerMode: 'none'
    };
  };

  selectPhotoTapped() {
    const options = {
      title: '写真を選ぶ',
      storageOptions: {
        skipBackup: true,
        path: 'images'
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response.uri };
        console.log('source', source);

        ImageResizer.createResizedImage(source.uri, 400, 300, 'JPEG', 80)
        .then(({uri}) => {
          let resizeSource = { uri: uri };
          this.setState({
            ImageSource: resizeSource,
          });
        }).catch((err) => {
          console.log(err);
          return Alert.alert('Unable to resize the photo',
            'Check the console for full the error message');
        });
      }
    });
  }

  render() {
    return (
      <Screen>
        <KeyboardAwareScrollView>
          <CardSection>
           <TouchableOpacity
             style={styles.frame}
             onPress={this.selectPhotoTapped.bind(this)}
           >
             { this.state.ImageSource === null ? (
               <View style={styles.image}>
                 <Icon name="md-camera" style={[styles.icon, { color: '#fff', fontSize: 50 }]} />
               </View>
             ) : (
               <View>
                 <Image style={styles.sample} source={this.state.ImageSource} />
               </View>
             )}
           </TouchableOpacity>
          </CardSection>
          <CardSection>
           <Input
             label={'アイテム名'}
             description={'アイテムの説明が入ります。'}
             placeholder={'アイテム名を入力してください'}
             value={this.props.name}
             onChangeText={value => this.props.itemUpdate({ prop: 'name', value })}
           />
          </CardSection>

          <CardSection>
           <Input
             label={'価格'}
             description={'価格の説明が入ります。'}
             placeholder={'価格を入力してください'}
             keyBoardType={'number-pad'}
             value={this.props.price}
             onChangeText={value => this.props.itemUpdate({ prop: 'price', value })}
           />
          </CardSection>

          <CardSection>
           <Box
             label={'詳細'}
             description={'詳細の説明が入ります。'}
             placeholder={'詳細を入力してください'}
             value={this.props.content}
             onChangeText={value => this.props.itemUpdate({ prop: 'content', value })}
           />
          </CardSection>
        </KeyboardAwareScrollView>
      </Screen>
    );
  }
}

const mapStateToProps = (state) => {
  const { name, price, content, imageuri } = state.itemForm;
  return { name, price, content, imageuri };
};

export default connect(mapStateToProps, {
  itemUpdate,
  itemCreate
})(Create);
