import React from 'react';
import { Keyboard, Platform, StyleSheet, View, Text, Image, ImageBackground } from 'react-native';
import firebase from 'react-native-firebase';

import SocialAuth from '../../auth/social/components/SocialAuth';
import Button from '../../ui/components/Button';
import Icon from '../../ui/components/Icon';
import * as Theme from '../../theme';

import Logo from '../../../assets/logo.png';
import Background from '../../../assets/background.png';

type Props = {
  title: string,
}

type State = {
  hidden: boolean,
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: null,
    height: null,
    padding: 15
  },
  logo: {
    width: 90,
    height: 90,
    marginBottom: 30
  },
  logoText: {
    color: '#fff',
    fontSize: 32,
    fontWeight: 'bold'
  },
  logoContainer: {
    flex: 1,
    alignItems: 'center',
    paddingTop: 60
  },
  iconContainer: {
    backgroundColor: '#fff',
  },
  icon: {
    fontSize: 28,
    marginRight: 0
  },
  iconStyle: {
    color: Theme.PRIMARY,
  },
  optionsContainer: {
    alignItems: 'center',
    height: 300,
    padding: 24
  },
  optionsContainerHidden: {
    height: 0,
  },
  socialTitle: {
    color: '#000',
    fontSize: 16,
    fontWeight: '500',
    marginBottom: 16,
  },
  facebook: {
    backgroundColor: '#4267b2',
    marginTop: 10,
    marginBottom: 10
  },
});


export default class Login extends React.Component {
  keyboardDidHideObserver: any;
  keyboardDidShowObserver: any;

  constructor(props: Props, context: any) {
    super(props, context);
    this.state = {
      hidden: false,
    };
  }

  componentDidMount() {
    if (Platform.OS === 'android') {
      this.keyboardDidShowObserver = Keyboard.addListener('keyboardDidShow', this.onKeyboardDidShow);
      this.keyboardDidHideObserver = Keyboard.addListener('keyboardDidHide', this.onKeyboardDidHide);
    }
  }

  componentWillUnmount() {
    if (Platform.OS === 'android') {
      if (this.keyboardDidShowObserver) this.keyboardDidShowObserver.remove();
      if (this.keyboardDidHideObserver) this.keyboardDidHideObserver.remove();
    }
  }

  render() {
    const { hidden } = this.state;
    return (
      <ImageBackground
        source={Background}
        style={styles.container}
        >
        <View style={styles.logoContainer}>
          <Image
            source={Logo}
            style={styles.logo}
          />
          <Text style={styles.logoText}>Market Native</Text>
        </View>

        <SocialAuth
          onSuccess={this.onSuccess}
          providerId={firebase.auth.FacebookAuthProvider.PROVIDER_ID}
          render={onPress => (
            <Button
              onPress={onPress}
              icon="logo-facebook"
              text="Facebookでログインする"
              iconStyle={styles.icon}
              containerStyle={styles.facebook}
            />
          )}
          type="signIn"
        />
      </ImageBackground>
    );
  }

  onKeyboardDidShow = () => {
    this.setState({ hidden: true });
  }

  onKeyboardDidHide = () => {
    this.setState({ hidden: false });
  }

  onSuccess = (user: Object, providerId: string) => {
    firebase.analytics().logEvent('login', {
      sign_up_method: providerId,
    });

    firebase.database().ref(`/users/${user.uid}`)
    .update({
      name: user.displayName,
      photoURL: user.photoURL,
      uid: user.uid,
    });
  }
}
