import { createStackNavigator } from 'react-navigation';

// screens
import Login from './Login';

export default createStackNavigator({
  Login: { screen: Login },
}, {
  initialRouteName: 'Login',
  headerMode: 'none'
});
