import { createStore, applyMiddleware, combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import reduxThunk from 'redux-thunk';

import itemReducer from '../ui/redux/itemReducer';
import itemFormReducer from '../ui/redux/itemFormReducer';

import dealReducer from '../ui/redux/dealReducer.js';
import dealFormReducer from '../ui/redux/dealFormReducer';

import roomReducer from '../ui/redux/roomReducer';
import roomFormReducer from '../ui/redux/roomFormReducer';

import messageReducer from '../ui/redux/messageReducer';
import messageFormReducer from '../ui/redux/messageFormReducer';

import payFormReducer from '../ui/redux/payFormReducer';

import accountFormReducer from '../ui/redux/accountFormReducer';

import transferReducer from '../ui/redux/transferReducer';

import uiReducer from '../ui/redux/uiReducer';

const rootReducer = combineReducers({
  ui: uiReducer,
  items: itemReducer,
  itemForm: itemFormReducer,
  deals: dealReducer,
  dealForm: dealFormReducer,
  rooms: roomReducer,
  roomForm: roomFormReducer,
  messages: messageReducer,
  messageForm: messageFormReducer,
  payForm: payFormReducer,
  accountForm: accountFormReducer,
  transfers: transferReducer
});

const store = createStore(
  rootReducer,
  applyMiddleware(reduxThunk)
);
export default store;
